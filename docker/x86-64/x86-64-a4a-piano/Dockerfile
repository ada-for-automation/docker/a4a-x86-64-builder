
# Ada for Automation Registry
ARG A4A_REGISTRY=registry.gitlab.com/ada-for-automation/docker

# Web Dev image
ARG A4A_WEB_DEV_IMG=a4a-x86-64-builder/x86-64-a4a-web-dev:latest

# Use latest Ada for Automation Web Development as build image
FROM $A4A_REGISTRY/$A4A_WEB_DEV_IMG AS build

# Use latest debian as base image
FROM debian:bookworm AS base

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian (bookworm) / A4A_PIANO"

# Version
ENV A4A_PIANO=0.0.1

# Install required libraries
RUN apt-get update  \
    && apt-get install -y \
    libgnat-12 libmodbus5

# Application path
ARG LOCATION="/home/pi/Ada/A4A/demo/010 a4a_piano"

# Install Ada for Automation demo application and libraries
COPY --from=build \
    $LOCATION/bin/A4A_Piano \
    $LOCATION/bin/

# Install Ada for Automation demo application files
COPY --from=build \
    $LOCATION/css \
    $LOCATION/html \
    $LOCATION/img \
    $LOCATION/js \
    $LOCATION/

WORKDIR $LOCATION

# The entrypoint shall start the demo application
ENTRYPOINT ["./bin/A4A_Piano", "--log-level=info", "--log-color=no"]

# Modbus TCP Server port
EXPOSE 1504

# Web Server Port
EXPOSE 8081

# Set STOPSIGNAL
STOPSIGNAL SIGINT


