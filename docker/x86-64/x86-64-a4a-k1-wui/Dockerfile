
# Ada for Automation Registry
ARG A4A_REGISTRY=registry.gitlab.com/ada-for-automation/docker

# Web Dev image
ARG A4A_WEB_DEV_IMG=a4a-x86-64-builder/x86-64-a4a-web-dev:latest

# Use latest Ada for Automation Web Development as build image
FROM $A4A_REGISTRY/$A4A_WEB_DEV_IMG AS build

# Build the Ada for Automation Web demo application
RUN cd "/home/pi/Ada/A4A/demo/022 a4a-k1-wui" && Arch=x86_64 make

# Use latest debian as base image
FROM debian:bookworm AS base

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian (bookworm) / A4A-K1-WUI"

# Version
ENV A4A_K1_WUI=0.0.1

# Install required libraries
RUN apt-get update  \
    && apt-get install -y \
    libgnat-12 libmodbus5

# Application path
ARG LOCATION="/home/pi/Ada/A4A/demo/022 a4a-k1-wui"

# Install Ada for Automation demo application
COPY --from=build \
    $LOCATION/bin/a4a_k1_wui_demo \
    $LOCATION/bin/

# Install Ada for Automation demo application files
COPY --from=build \
    $LOCATION/css \
    $LOCATION/html \
    $LOCATION/img \
    $LOCATION/js \
    $LOCATION/

WORKDIR $LOCATION

# The entrypoint shall start the demo application
ENTRYPOINT ["./bin/a4a_k1_wui_demo", "--log-level=info", "--log-color=no"]

# Modbus TCP Server port
EXPOSE 1503

# Web Server Port
EXPOSE 8080

# Set STOPSIGNAL
STOPSIGNAL SIGINT


